package com.nortal.gatling.scenarios

import com.nortal.gatling.requests.{CreateUserRequest, GetTokenRequest}
import io.gatling.core.Predef.scenario

object CreateUserScenario {
  val createUserScenario = scenario("Create User Scenario")
    /*  Calling pre-defined requests  */
    .exec(GetTokenRequest.get_token)
    .exec(CreateUserRequest.create_user)
}